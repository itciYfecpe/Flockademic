jest.mock('../../src/services/addHeaderImageToPeriodical', () => ({
  addHeaderImageToPeriodical: jest.fn().mockReturnValue(Promise.resolve()),
}));
jest.mock('../../../../lib/lambda/getUploadUrl', () => ({
  getUploadUrl: jest.fn().mockReturnValue('Arbitrary URL'),
}));
jest.mock('../../src/utils/canManagePeriodical', () => ({
  canManagePeriodical: jest.fn().mockReturnValue(Promise.resolve(true)),
}));

import { generateAssetUploadUrl } from '../../src/resources/generateAssetUploadUrl';

const mockContext = {
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  body: { result: { image: 'arbitrary_filename.png' }, targetCollection: { identifier: 'arbitrary_id' } },
  headers: {},
  method: 'POST' as 'POST',
  params: [ '/arbitrary_id/upload/header', 'arbitrary_id' ],
  path: '/arbitrary_id/upload/header',
  query: null,
};

beforeEach(() => {
  process.env.asset_upload_bucket = 'arbitrary_bucket_name';
});

it('should error when no account could be verified', () => {
  const promise = generateAssetUploadUrl({ ...mockContext, session: new Error('Arbitrary error') });

  return expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

it('should error when no bucket to upload to was configured in the environment, and log it', () => {
  console.log = jest.fn();
  delete process.env.asset_upload_bucket;

  const promise = generateAssetUploadUrl(mockContext);

  expect(console.log.mock.calls.length).toBe(1);
  expect(console.log.mock.calls[0][0]).toBe('No upload bucket defined.');
  return expect(promise).rejects.toEqual(new Error('There was a problem uploading your image.'));
});

it('should error when no periodical ID was specified', () => {
  const promise = generateAssetUploadUrl({
    ...mockContext,
    body: {
      result: { image: 'arbitrary_filename.png' },
      targetCollection: {},
    } as any,
    params: [],
    path: '/' ,
  });

  return expect(promise).rejects.toEqual(new Error('No journal for which to upload an image specified.'));
});

it('should error when the given periodical is not managed by the current user', () => {
  const mockCanManagePeriodical = require.requireMock('../../src/utils/canManagePeriodical').canManagePeriodical;
  mockCanManagePeriodical.mockReturnValueOnce(false);

  const promise = generateAssetUploadUrl(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only set the header image of journals you manage.'));
});

it('should error when no filename was specified', () => {
  const promise = generateAssetUploadUrl({
    ...mockContext,
    body: {
      result: {},
      targetCollection: { identifier: 'Arbitrary identifier' },
    } as any,
    params: [],
    path: '/' ,
  });

  return expect(promise).rejects.toEqual(new Error('No journal for which to upload an image specified.'));
});

it('should return the generated URL when all parameters are correct', () => {
  const mockedGetUploadUrl = require.requireMock('../../../../lib/lambda/getUploadUrl').getUploadUrl;
  mockedGetUploadUrl.mockReturnValueOnce('Some URL');

  const promise = generateAssetUploadUrl({
    ...mockContext,
    body: {
      result: { image: 'some_filename.png' },
      targetCollection: { identifier: 'some_id' },
    },
    params: [ '/some_id/upload/header', 'some_id' ],
    path: '/some_id/upload/header',
    session: { identifier: 'Some creator' },
  });

  return expect(promise).resolves.toEqual({
    object: { toLocation: 'Some URL' },
    result: {
      image: expect.stringContaining('some_filename.png'),
    },
  });
});
